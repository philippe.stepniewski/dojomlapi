import os

import streamlit as st
import requests
from PIL import Image
import glob
import os

st.title("Recherche de visage dans une base de données")
st.write("Chargez une image:")


def display_image_list(res):
    json_data = res.json()
    data = json_data["prediction"].replace('"', '')
    data = data.replace("[", "")
    data = data.replace("]", "")
    data = data.replace("'", "")
    data = data.replace(" ", "")
    array = data.split(',')
    all_images = []
    for file_name in array:
        img_path = glob.glob('./images/**/' + file_name + "*", recursive=True)
        image = Image.open(img_path[0])
        all_images.append(image)
    st.image(all_images, width=200)


image = st.file_uploader("Choose an image")
if image is not None:
    files = {'img_file': ("image", image, "image/jpeg")}
    # TODO Ecrire le code executant l'appel à l'API de recherche
    res = None
    image = Image.open(image)
    st.write("L'image en requête:")
    st.image(image, width=200)
    st.write("Les résultats:")
    display_image_list(res)
