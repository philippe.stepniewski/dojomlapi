from model_serving.indexer.image_index import ImageIndex


if __name__ == "__main__":
    indexer = ImageIndex()
    # TODO ecrire le code permettant d'indexer toutes le photos d'un dossier et de stocker l'index sur disque
    indexer.dump_index_to_disk("index_2", "indexes")
