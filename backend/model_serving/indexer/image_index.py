from annoy import AnnoyIndex
import numpy as np
from model_serving.utils.file_management import parse_folder_subfolders
from tensorflow import keras
import pickle
import os
from PIL import Image


class ImageIndex:

    def __init__(self):
        self.id_map = None
        self.input_shape = None
        self.index: AnnoyIndex = None
        self.indexer: keras.Model  = None
        self.index_path: str = None
        self.feature_size: int = None

    def init_index(self):
        index = AnnoyIndex(self.feature_size, "angular")
        self.index = index


    def prepare_feature_extractor(self, model):
        feature_extractor = keras.Model(
            inputs=model.inputs,
            outputs=model.get_layer(name="feature_layer").output,
        )
        self.input_shape = (model.input_shape[1], model.input_shape[2])
        self.feature_size = model.get_layer(name="feature_layer").output_shape[1]
        self.indexer = feature_extractor

    def load_tf_model_from_disk(self, model_path: str):
        model = keras.models.load_model(model_path)
        self.prepare_feature_extractor(model)

    def extract_features(self, image: np.array):
        # TODO ecrire le code permettant d'extraite les embeddings d'une images
        image_features = None
        return image_features

    def add_to_index(self, image_features: np.array, image_id: id):
        self.index.add_item(image_id, image_features)

    def index_image(self, image: np.array, image_id: np.array):
        image_features = self.extract_features(image)
        nb_images = len(image_features)
        for i in range(0, nb_images):
            self.add_to_index(image_features[i], image_id[i])

    def query_image_from_path(self, image_path: str):
        image = self.load_image(image_path)
        image_features = self.extract_features(image)
        similar_ids = self.search_index(image_features)
        return similar_ids

    def query_image(self, image):
        image = image.resize(self.input_shape, Image.NEAREST).convert("RGB")
        image = np.array([keras.utils.img_to_array(image) / 255.0])
        #TODO écrire le code permettant de rechercher une image dans l'index
        similar_ids = None
        return similar_ids

    def search_index(self, image_features: np.array, knn: int = 10):
        similar_ids = self.index.get_nns_by_vector(image_features[0], n=knn)
        real_ids = self.get_real_ids_from_index(similar_ids)
        return real_ids

    def get_real_ids_from_index(self, ids: list):
        res = []
        for id in ids:
            res.append(self.id_map[id])
        return res

    def load_index_from_disk(self, index_path: str):
        self.index_path = index_path
        print(index_path + "/index.dat")
        self.index.load(index_path + "/index.dat")
        self.load_id_map(index_path)

    def dump_index_to_disk(self, name_id: str, index_path: str):
        final_path = index_path + "/" + name_id
        path_exists = os.path.exists(final_path)
        if not path_exists:
            os.makedirs(final_path)
        self.index.build(500)
        self.index_path = final_path
        self.index.save(self.index_path + "/index.dat")
        self.save_id_map()

    def index_images_in_folder(self, path_to_index: str, image_extension: str, batch_size: int):
        image_list = parse_folder_subfolders(path_to_index, image_extension)
        cpt = 0
        image_array = []
        image_id_array = []
        self.id_map = {}
        for image in image_list:
            image_path = image[0]
            image_id = image[1]
            self.id_map[cpt] = image_id
            image = self.load_image(image_path)
            image_array.append(image)
            image_id_array.append(cpt)
            cpt += 1
            if (cpt % batch_size) == 0:
                image_stack = np.vstack(image_array)
                #TODO compléter le code permettant d'indéxer ce batch
                image_array = []
                image_id_array = []
        if len(image_array) > 0:
            image_stack = np.vstack(image_array)
            #TODO compléter le code permettant d'indéxer ce batch

    def load_image(self, image_path):
        image = keras.utils.load_img(image_path, target_size=self.input_shape)
        image = np.array([keras.utils.img_to_array(image) / 255.0])
        return image

    def save_id_map(self):
        f = open(self.index_path + "/id_map.dat", "wb")
        pickle.dump(self.id_map, f)
        f.close()

    def load_id_map(self, id_map_path: str):
        f = open(id_map_path + "/id_map.dat", "rb")
        self.id_map = pickle.load(f)
        f.close()
