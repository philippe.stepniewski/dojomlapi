import keras
import pytest
import tensorflow as tf
from model_serving.indexer.image_index import ImageIndex
import numpy as np

FEATURE_SIZE = 4096
PATH_TO_MODEL = "../keras_model/deepface_base_model.h5"

@pytest.fixture()
def features():
    return np.random.rand(FEATURE_SIZE)


@pytest.fixture
def image():
    image = tf.keras.utils.load_img("data/images/Daniel_Radcliffe/dr_1.jpg", target_size=(152, 152))
    input_arr = tf.keras.utils.img_to_array(image)
    input_arr = np.array([input_arr])
    return input_arr


def test_load_tf_model_from_disk_should_load_the_model():
    indexer = ImageIndex()
    indexer.load_tf_model_from_disk(PATH_TO_MODEL)
    assert (type(indexer.indexer) == keras.engine.functional.Functional)


def test_extract_features_should_be_correct_size_and_not_0(image):
    indexer = ImageIndex()
    indexer.load_tf_model_from_disk(PATH_TO_MODEL)
    res = indexer.extract_features(image)
    assert (len(res[0]) == indexer.feature_size)
    assert (res[0].sum() > 0)


def test_init_index_should_initiate_an_index():
    indexer = ImageIndex()
    indexer.load_tf_model_from_disk(PATH_TO_MODEL)
    indexer.init_index()
    assert (True)


def test_add_to_index_should_add_with_correct_id(features):
    indexer = ImageIndex()
    indexer.load_tf_model_from_disk(PATH_TO_MODEL)
    indexer.init_index()
    image_id = 0
    features = np.array(features)
    indexer.add_to_index(features, image_id)
    len_index = indexer.index.get_n_items()
    assert (len_index == 1)


def test_index_images_in_folder_should_index_all_images_and_save_on_disk():
    indexer = ImageIndex()
    indexer.load_tf_model_from_disk(PATH_TO_MODEL)
    indexer.init_index()
    indexer.index_images_in_folder("data/images", image_extension=".jpg", batch_size=2)
    len_index = indexer.index.get_n_items()
    indexer.dump_index_to_disk("test_index_1", "indexes")
    assert (len_index == 9)
