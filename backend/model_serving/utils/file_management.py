import os
import pandas as pd


class ColumnNames:
    image_path = "image_path"
    image_id = "id"


def parse_folder_subfolders(path_to_parse: str, image_extension: str) -> [str]:
    image_list = []
    for root, dirs, files in os.walk(path_to_parse):
        for file in files:
            if file.endswith(image_extension):
                image_path = os.path.join(root, file)
                basename = os.path.basename(file)
                basename = basename.replace(image_extension, "")
                image_list.append((image_path, basename))
    return image_list


def create_dataframe_from_folder(path_to_parse: str, image_extension: str, file_with_labels: str,
                                 label_column: str) -> pd.DataFrame:
    image_list = parse_folder_subfolders(path_to_parse, image_extension)
    res = pd.DataFrame(image_list)
    res.columns = [ColumnNames.image_path, ColumnNames.image_id]
    res[ColumnNames.image_id] = res[ColumnNames.image_id].astype(int)
    image_labels = pd.read_csv(file_with_labels, sep=";")
    flow_df = res.merge(image_labels[[ColumnNames.image_id, label_column]], on=ColumnNames.image_id)
    return flow_df
