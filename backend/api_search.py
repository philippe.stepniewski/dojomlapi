from io import BytesIO
import uvicorn
from model_serving.indexer.image_index import ImageIndex
from PIL import Image
from fastapi import FastAPI
from fastapi import File
from fastapi import UploadFile


app = FastAPI()

# TODO Ecrire le code de la fonction qui execute une recherche suite à un appel sur /search de l'API
@app.post("/search/")
async def get_image(img_file: UploadFile = File(...)):
    request_object_content = await img_file.read()
    image = Image.open(BytesIO(request_object_content))

    results = None
    return {"prediction": str(results)}


if __name__ == "__main__":
    uvicorn.run("api_search:app", host="0.0.0.0", port=8081)
