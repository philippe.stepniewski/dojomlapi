SHELL := /bin/bash
.SHELLFLAGS = -ec
.ONESHELL:
.SILENT:


.PHONY: help
help:
	echo "❓ Use \`make <target>'"
	grep -E '^\.PHONY: [a-zA-Z0-9_-]+ .*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = "(: |##)"}; {printf "\033[36m%-30s\033[0m %s\n", $$2, $$3}'

.PHONY: dependencies ## Installe les dépendances de dev
dependencies:
	python -m pip install -r requirements.txt
	python -m pip install -r backend/api_requirements.txt


.PHONY: get-data ## Récupère les données sur google drive pour le dojo
get-data:
	gdown 1Ym-Z5tikmzTzbrwhBpl73eWr0Y6ebmtq

.PHONY: deploy ## 🕵 Starts the service
deploy:
	docker-compose up -d --build

.PHONY: test ## Launches the tests
	python -m pytest backend/test/test_FashionCNN.py
	python -m pytest backend/test/test_index.py
