# PYTHON 3.10 Recommandé 


### Installez les dépendances

    make dependencies

### Récupérer les données sur gdrive

    make get-data

- Copiez le dossier data à la racine du projet
- Copiez le modèle keras au bon endroit

### Installer tensorflow sur Mac M1

    https://developer.apple.com/metal/tensorflow-plugin/

## A VOUS DE JOUER

Complétez tous les trous #TODO pour avoir une appli qui fonctionne
